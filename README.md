# Test task for abstract company

## Task
* Создать простой сайт, где будет страница с формой для авторизации и страница для авторизованного пользователя.
* На странице с авторизацией сделать форму (login / password).
* На странице для уже авторизированного пользователя реализовать следующие пункты: 
1. Создать дерево разделов, где каждый раздел имеет название и краткое описание. 
2. Каждый раздел может иметь неограниченное число подразделов, которые, в свою очередь, могут также иметь свои подразделы. 
3. Информацию о любом разделе или подразделе можно редактировать. 
4. Разделы и подразделы можно удалять на любом уровне вложенности. 
* Вид дерева можно использовать любой на Ваше усмотрение. Для хранения данных используйте MySQL.
* Дизайн, цвет, шрифт, размер текста, отступы и т.п. всё на Ваш вкус. Адаптивная разметка с использованием html5 и css3 будет считаться плюсом.
* Использование готовых фреймворков PHP запрещено. Будет проверяться организация кода и Ваш подход к выполнению задачи.
* Выполненное решение прислать как zip архив или ссылку на github/bitbucket. В письме указать сколько времени Вы затратили на решение данной задачи.

## Requirements
* Php 7.3
* Mysql or MariaDB, one of the last versions (all used SQL requests are really simple)
* Composer

## Setup
* Set **/public** folder as DocumentRoot and make sure that web-server redirects all requests to index.php, e.g. the following settings will work on Apache2:
    ```
    ServerName localhost
    DocumentRoot /var/www/html/abstract/public
    DirectoryIndex /index.php
    <Directory /var/www/html/abstract/public>
        AllowOverride None
        Order Allow,Deny
        Allow from All
        FallbackResource /index.php
    </Directory>
    ```
* Clone repo
* Run `composer install`
* Open [App/Config.php](App/Config.php) and enter your database configuration data.
* Run dump.sql in database console

## Run app
* http://localhost - start-up page
* test credentials - username "admin", password "admin"
